﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = (Split-Path -Leaf $MyInvocation.MyCommand.Path).Replace(".Tests.", ".")
. "$here\$sut"

Describe "Test-Scheduling" {
	
	$sched = New-Object psobject -Property @{"year"="*";"month"="*";"dayOfMonth"="*";"dayOfWeek"="*";"lastRunDate"=""}

	Context "DayOfWeek" {
		It "can filter a single day of week" {
			$sched.dayOfWeek = "2"
			$date = [datetime]"06/28/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $true
			$date = [datetime]"06/29/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $false
			$sched.DayOfWeek = "*"
		}
		
		It "can filter a range of dayOfWeek" {
			$sched.dayOfWeek = "2-5"
			$date = [datetime]"06/27/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $false
			$date = [datetime]"06/28/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $true
			$date = [datetime]"06/29/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $true
			$date = [datetime]"06/30/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $true
			$date = [datetime]"07/1/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $true
			$date = [datetime]"07/2/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $false
			$date = [datetime]"07/3/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $false
			$sched.DayOfWeek = "*"
		}
		
		It "can filter a range of dayOfWeek - reversed" {
			$sched.dayOfWeek = "5-2"
			$date = [datetime]"06/27/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $true
			$date = [datetime]"06/28/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $true
			$date = [datetime]"06/29/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $false
			$date = [datetime]"06/30/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $false
			$date = [datetime]"07/1/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $true
			$date = [datetime]"07/2/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $true
			$date = [datetime]"07/3/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $true
			$sched.DayOfWeek = "*"
		}
		
		It "can filter on a subset of day of week" {
			$sched.dayOfWeek = "2,4,7"
			$date = [datetime]"06/27/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $false
			$date = [datetime]"06/28/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $true
			$date = [datetime]"06/29/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $false
			$date = [datetime]"06/30/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $true
			$date = [datetime]"07/1/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $false
			$date = [datetime]"07/2/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $false
			$date = [datetime]"07/3/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $true
			$sched.DayOfWeek = "*"
		}
	}
	
	Context "DayOfMonth" {
		It "can filter on a specific day of month" {
			$sched.dayOfMonth = "20"
			$date = [datetime]"07/01/2016"
			foreach($day in 0..30){
				if($day -eq 19){
					Test-Scheduling -scheduling $sched -dateToTest $date.AddDays($day) | Should Be $true
				} else {
					Test-Scheduling -scheduling $sched -dateToTest $date.AddDays($day) | Should Be $false
				}
			}
			$sched.dayOfMonth = "*"
		}
		
		It "can filter on a range of day of month" {
			$sched.dayOfMonth = "4-23"
			$date = [datetime]"07/01/2016"
			foreach($day in 0..30){
				if(3..22 -contains $day){
					Test-Scheduling -scheduling $sched -dateToTest $date.AddDays($day) | Should Be $true
				} else {
					Test-Scheduling -scheduling $sched -dateToTest $date.AddDays($day) | Should Be $false
				}
			}
			$sched.dayOfMonth = "*"
		}
		
		It "can filter on a subset of day of month" {
			$sched.dayOfMonth = "2,10,14,30,25"
			$date = [datetime]"07/01/2016"
			foreach($day in 0..30){
				if(@(1,9,13,29,24) -contains $day){
					Test-Scheduling -scheduling $sched -dateToTest $date.AddDays($day) | Should Be $true
				} else {
					Test-Scheduling -scheduling $sched -dateToTest $date.AddDays($day) | Should Be $false
				}
			}
			$sched.dayOfMonth = "*"
		}
	}
	
	Context "Month" {
		It "can filter on a specific month" {
			$sched.month = "6"
			$date = [datetime]"07/02/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $false
			Test-Scheduling -scheduling $sched -dateToTest $date.AddMonths(-1) | Should Be $true
			Test-Scheduling -scheduling $sched -dateToTest $date.AddMonths(-2) | Should Be $false
			Test-Scheduling -scheduling $sched -dateToTest $date.AddMonths(-1).AddYears(1) | Should Be $true
			$sched.month = "*"
		}
		
		It "can filter on a range of months" {
			$sched.month = "5-11"
			$date = [datetime]"01/02/2016"
			foreach($month in 0..11){
				if(4..10 -contains $month){
					Test-Scheduling -scheduling $sched -dateToTest $date.AddMonths($month) | Should Be $true
				} else {
					Test-Scheduling -scheduling $sched -dateToTest $date.AddMonths($month) | Should Be $false
				}
			}
			$sched.month = "*"
		}
		
		It "can filter on a range of months - reversed" {
			$sched.month = "11-5"
			$date = [datetime]"01/02/2016"
			foreach($month in 0..11){
				if(@(10,11,0,1,2,3,4) -contains $month){
					Test-Scheduling -scheduling $sched -dateToTest $date.AddMonths($month) | Should Be $true
				} else {
					Test-Scheduling -scheduling $sched -dateToTest $date.AddMonths($month) | Should Be $false
				}
			}
			$sched.month = "*"
		}
		
		It "can filter on a subset of months" {
			$sched.month = "2,5,7,12"
			$date = [datetime]"01/02/2016"
			foreach($month in 0..11){
				if(@(1,4,6,11) -contains $month){
					Test-Scheduling -scheduling $sched -dateToTest $date.AddMonths($month) | Should Be $true
				} else {
					Test-Scheduling -scheduling $sched -dateToTest $date.AddMonths($month) | Should Be $false
				}
			}
			$sched.month = "*"
		}
	}
	
	Context "Year" {
		It "can filter on a specific year" {
			$sched.year = "2016"
			$date = [datetime]"07/02/2016"
			Test-Scheduling -scheduling $sched -dateToTest $date | Should Be $true
			Test-Scheduling -scheduling $sched -dateToTest $date.AddYears(-1) | Should Be $false
			Test-Scheduling -scheduling $sched -dateToTest $date.AddYears(-2) | Should Be $false
			Test-Scheduling -scheduling $sched -dateToTest $date.AddYears(1) | Should Be $false
			$sched.year = "*"
		}
		
		It "can filter on a range of years" {
			$sched.year = "2014-2016"
			$date = [datetime]"01/02/2012"
			foreach($year in 0..10){
				if(2..4 -contains $year){
					Test-Scheduling -scheduling $sched -dateToTest $date.AddYears($year) | Should Be $true
				} else {
					Test-Scheduling -scheduling $sched -dateToTest $date.AddYears($year) | Should Be $false
				}
			}
			$sched.year = "*"
		}
		
		It "can filter on a range of years - reversed" {
			$sched.year = "2016-2014"
			$date = [datetime]"01/02/2012"
			foreach($year in 0..10){
				if(@(2..4) -contains $year){
					Test-Scheduling -scheduling $sched -dateToTest $date.AddYears($year) | Should Be $true
				} else {
					Test-Scheduling -scheduling $sched -dateToTest $date.AddYears($year) | Should Be $false
				}
			}
			$sched.year = "*"
		}
		
		It "can filter on a subset of years" {
			$sched.year = "2015,2017,2018"
			$date = [datetime]"01/02/2012"
			foreach($year in 0..10){
				if(@(3,5,6) -contains $year){
					Test-Scheduling -scheduling $sched -dateToTest $date.AddYears($year) | Should Be $true
				} else {
					Test-Scheduling -scheduling $sched -dateToTest $date.AddYears($year) | Should Be $false
				}
			}
			$sched.year = "*"
		}
	}
}
