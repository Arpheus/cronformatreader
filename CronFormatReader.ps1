function Test-Scheduling{
	[CmdletBinding(DefaultParameterSetName="none")]
	Param(
		[Parameter(Mandatory=$true)]
		[object]
		[ValidateNotNullOrEmpty()]
		$scheduling,
		
		[Parameter(Mandatory=$false)]
		[Datetime]
		[ValidateNotNullOrEmpty()]
		$dateToTest = $([Datetime]::now)
	)
	
	#Tips : as we don't focus on execution time (hours and minutes), set the time of $lastRunDate and $dateToTest to midnight.
	$lastRunDate = $null
	if($scheduling.lastRunDate -ne ""){
		$lastRunDate = [datetime]::ParseExact($scheduling.lastRunDate, "MM/dd/yyyy", $null)
	}
	$dateToTest = $dateToTest.Date
	
	$dfi = [System.Globalization.DateTimeFormatInfo]::CurrentInfo
	$cal = $dfi.Calendar
	
	#year
	if($scheduling.year -eq "*"){
		#nothing to do, all years are matching
		#Saving time doing nothing ;)
	} elseif($scheduling.year -match "^\d{4}(,\d{4})*$"){
		#specific year(s), test it
		[int[]]$years = @($scheduling.year -split ",")
		if($years -contains $dateToTest.year){
			#year is matching!
		} else {
			return $false
		}
	} elseif($scheduling.year -match "^\*/\d+$"){
		#Recurring schedule
		if($lastRunDate){
			[int]$recurrence = $($scheduling.year -replace "\*\/","")
			#test against lastRunDate
			$intervalyears = $dateToTest.Year - $lastRunDate.Year
			if($intervalYears -ge $recurrence){
				#match !
			} else {
				return $false
			}
		} else {
			#No lastRunDate, consider it as a match
		}
	} elseif($scheduling.year -match "^\d{4}-\d{4}$"){
		#Range of years
		[int[]]$bounds = @($scheduling.year -split "-")
		$years = $bounds[0]..$bounds[1]
		if($years -contains $dateToTest.year){
			#Match !
		} else {
			return $false
		}
	} else {
		throw "Incorrect dayOfWeek"
		return $false
	}
	
	#month
	if($scheduling.month -eq "*"){
		#nothing to do, all months are matching
		#Saving time doing nothing ;)
	} elseif($scheduling.month -match "^\d{1,2}(,\d{1,2})*$"){
		#specific month(s), test it
		[int[]]$months = @($scheduling.month -split ",")
		Write-Verbose $($months -join "  ")
		if($months -contains $dateToTest.month){
			#month is matching!
		} else {
			return $false
		}
	} elseif($scheduling.month -match "^\*/\d+$"){
		#Recurring schedule
		if($lastRunDate){
			#test against lastRunDate
			[int]$recurrence = $($scheduling.month -replace "\*\/","")
			$intervalMonths = [System.Math]::Abs((12 - $dateToTest.Month) - (12 - $lastRunDate.Month))
			$interval = $dateToTest - $lastRunDate
			$intervalMonths += 12 * [System.Math]::Round($interval.TotalDays/364)
			if($intervalMonths -ge $recurrence){
				#match !
			} else {
				return $false
			}
		} else {
			#No lastRunDate, consider it as a match
		}
	} elseif($scheduling.month -match "^\d{1,2}-\d{1,2}$"){
		#Range of months
		[int[]]$bounds = @($scheduling.month -split "-")
		$months = $null
		if(-not ((1..12 -contains $bounds[0]) -and (1..12 -contains $bounds[1]))){
			#incorrect month
			throw "Incorrect month synthax"
			return $false
		}
		if($bounds[0] -gt $bounds[1]){
			$months = 1..12 | ?{$bounds[1]..$bounds[0] -notcontains $_}
			$months += $bounds
		} else {
			$months = $bounds[0]..$bounds[1]
		}
		if($months -contains $dateToTest.month){
			#Match !
		} else {
			return $false
		}
	} else {
		throw "Incorrect dayOfWeek"
		return $false
	}
	
	<#weekOfMonth
	if($scheduling.weekOfMonth -eq "*"){
		#nothing to do, all months are matching
		#Saving time doing nothing ;)
	} else {
		#Create an array with the weeks of the month
		$thisWeekOfYear = $cal.GetWeekOfYear($dateToTest, 0, [System.DayOfWeek]::Monday)
		$firstWeekOfMonth = $cal.GetWeekOfYear([datetime]"$($dateToTest.month)/01/$($dateToTest.year)", 0, [System.DayOfWeek]::Monday)
		$lastWeekOfMonth = $cal.GetWeekOfYear([datetime]"$($dateToTest.month)/$($cal.getDaysInMonth($dateToTest.year, $dateToTest.month))/$($dateToTest.year)", 0, [System.DayOfWeek]::Monday)
		$weeksOfMonth = $firstWeekOfMonth .. $lastWeekOfMonth
		
		if($scheduling.weekOfMonth -match "^[1-6](,[1-6])*$"){
			#Specific week(s)
			$weeks = $scheduling.weekOfMonth -split "," | %{$_ - 1}
			if($weeksOfMonth[$weeks] -contains $thisWeekOfYear){
				#match !
			} else {
				return $false
			}
		} elseif($scheduling.weekOfMonth -match "^[1-6]-[1-6]$"){
			#Range of weeks
			
			
		} else {
			#Unknown format
		}
		if($weeksOfMonth -contains $thisWeekOfYear){
			#Match !
		} else {
			return $false
		}
	}#>
	
	#dayOfMonth
	if($scheduling.dayOfMonth -eq "*"){
		#nothing to do, all days are matching
		#Saving time doing nothing ;)
	} elseif($scheduling.dayOfMonth -match "^\d{1,2}(,\d{1,2})*$"){
		#specific day(s) in month
		[int[]]$days = @($scheduling.dayOfMonth -split ",")
		if(	$days -contains $dateToTest.Day){
			#Match !
		} else {
			return $false
		}
	} elseif($scheduling.dayOfMonth -match "^\d{1,2}-\d{1,2}$"){
		Write-Verbose "dayOfMonth - range"
		[int[]]$bounds = $scheduling.dayOfMonth -split "-"
		$days = $null
		if(-not ((1..31 -contains $bounds[0]) -and (1..31 -contains $bounds[1]))){
			#incorrect day
			throw "Incorrect dayOfMonth synthax"
			return $false
		}
		Write-Verbose "$($bounds[0])  $($bounds[1])"
		if($bounds[0] -gt $bounds[1]){
			$days = 1..31 | ?{$bounds[1]..$bounds[0] -notcontains $_}
			$days += $bounds
		} else {
			$days = $bounds[0]..$bounds[1]
		}
		Write-Verbose $($days -join "  ")
		if($days -contains $dateToTest.Day){
			#Match !
		} else {
			return $false
		}
	} elseif($scheduling.dayOfMonth -match "^\*\/\d+$"){
		$recurrence = $scheduling.dayOfMonth -replace "\*\/",""
		$interval = $dateToTest - $lastRunDate
		if($interval.TotalDays -ge $recurrence){
			#Match !
		} else {
			return $false
		}
	} else {
		throw "Incorrect dayOfWeek"
		return $false
	}
	
	#dayOfWeek
	if($scheduling.dayOfWeek -eq "*"){
		#nothing to do, all days are matching
		#Saving time doing nothing ;)
	} elseif($scheduling.dayOfWeek -match "^\d{1}(,\d{1})*$"){
		[int[]]$days = $scheduling.dayOfWeek -split ","
		foreach($day in $days){
			if(($day -gt 7) -OR ($day -lt 1)){
				throw "Incorrect dayOfWeek"
				return $false
			}
		}
		$thisDayOfWeek = [int]$dateToTest.DayOfWeek
		if(@($days | %{$_%7}) -contains $thisDayOfWeek){
			#Match !
		} else {
			return $false
		}
	} elseif($scheduling.dayOfWeek -match "^\d{1,2}-\d{1,2}$"){
		[int[]]$bounds = $scheduling.dayOfWeek -split "-"
		$days = $null
		foreach($bound in $bounds){
			if(($bound -gt 7) -OR ($bound -lt 1)){
				throw "Incorrect dayOfWeek"
				return $false
			}
		}
		if($bounds[0] -gt $bounds[1]){
			$days = 1..7 | ?{$bounds[1]..$bounds[0] -notcontains $_}
			$days += $bounds
		} else {
			$days = $bounds[0]..$bounds[1]
		}
		$thisDayOfWeek = [int]$dateToTest.DayOfWeek
		if(@($days | %{$_%7}) -contains $thisDayOfWeek){
			#Match !
		} else {
			return $false
		}
	} else {
		throw "Incorrect dayOfWeek"
		return $false
	}
	
	return $true
}